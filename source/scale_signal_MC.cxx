#include "scale_signal_MC.h"
#include <iostream>
#include <fstream>

ROOT::RDF::RNode defineEventWeight(ROOT::RDF::RNode df){
  if(doLTreweighting == false){
    auto wgt = [=](float &wgt){
      double norm_weight = wgt/initalSumOfWeights;
      return norm_weight;
    };
    auto df_weights = df.Define("EventWeight", wgt, {"bothMuEventWeight"});
    return df_weights;
  }
  else{
    auto wgt = [=](float &wgt, float &ltwgt1, float &ltwgt2){   
      double norm_weight = (wgt*ltwgt1*ltwgt2)/initalSumOfWeights;
      return norm_weight;
    };

     auto lifetime_weight = [=](double &MuonParentProperLifetime) {
    if(MuonParentProperLifetime==-999){return (Float_t)0.;}
    else{      
      Float_t weight = (oldLifetime/newLifetime) * TMath::Exp( (MuonParentProperLifetime/oldLifetime) - (MuonParentProperLifetime/newLifetime) );
      return weight;
    }
  };

 auto df_lt = df.Define("lifetimeWeight1",lifetime_weight, {"Muon1ParentProperLifetime"}).
                 Define("lifetimeWeight2",lifetime_weight, {"Muon2ParentProperLifetime"});
 auto df_weights = df_lt.Define("EventWeight", wgt, {"bothMuEventWeight","lifetimeWeight1","lifetimeWeight2"});
    return df_weights;
  }
}

std::vector<TH2D> getNumEventsHists(std::string file, std::string cutsABCD, std::string cutsEFGH, float up_bin_edge){

  TH1::SetDefaultSumw2();  
  TFile * inFile = new TFile((file).c_str());

  auto treeName = "trees_MEDIUMD0_";  
  ROOT::RDataFrame df(treeName, file);

  TH1D * MetaData = (TH1D*)inFile->Get("MetaData_EventCount");
  initalSumOfWeights = MetaData->GetBinContent(3);

  auto df_cuts = df.Filter("passCuts");
  //todo lifetime weight, set to 1 if not required
  auto df_weighted = defineEventWeight(df_cuts);

  auto defPlusMu_f =  [](float mu1Charge, float mu1Var, float mu2Var) {
    if(mu1Charge==1){ return abs(mu1Var);}
    else{return abs(mu2Var);}
  };
  auto defNegMu_f =  [](float mu1Charge, float mu1Var, float mu2Var) {
    if(mu1Charge==-1){ return abs(mu1Var); }
    else{ return abs(mu2Var);}
  };
    
  auto df_charged_abs = df_weighted.Define("AbsMuonPlusTrackD0",defPlusMu_f,{"Muon1Charge","Muon1TrackD0","Muon2TrackD0"}).
    Define("AbsMuonNegTrackD0",defNegMu_f,{"Muon1Charge","Muon1TrackD0","Muon2TrackD0"});    
 
  Double_t bins[] = {0.1,0.3,0.6,up_bin_edge};
  int numBins = sizeof(bins)/sizeof(bins[0]) -1;

  auto df_cutsABCD = df_charged_abs.Filter(cutsABCD);
  TH2D histABCD = df_cutsABCD.Histo2D({"hist1", "",numBins,bins,numBins,bins},"AbsMuonPlusTrackD0","AbsMuonNegTrackD0","EventWeight").GetValue();
  
  auto df_cutsEFGH = df_charged_abs.Filter(cutsEFGH);
  TH2D histEFGH = df_cutsEFGH.Histo2D({"hist2", "",numBins,bins,numBins,bins},"AbsMuonPlusTrackD0","AbsMuonNegTrackD0","EventWeight").GetValue();

  std::vector<TH2D> output = {histABCD,histEFGH};
  return output;
}

TString getoutputNums(TString region){
  std::string cutsABCD = "";
  std::string cutsEFGH = "";
  std::string doTMcuts = "";
  if(doTruthMatching){
    doTMcuts = " && Muon1FirstParentTruthMatched && Muon2FirstParentTruthMatched";
  }
  float up_bin_edge = 0;
  TString datanums = "";
  if(region == "R1"){
    cutsABCD = "invMassTwoMuons < 200" + doTMcuts;
    cutsEFGH = "invMassTwoMuons > 200" + doTMcuts;
    up_bin_edge = 3;
    datanums = "2.06,0.25,2580.00,290.00,258.00,183.00,1.00";
  }
  if(region == "R2"){
    cutsABCD = "invMassTwoMuons < 140" + doTMcuts;
    cutsEFGH = "invMassTwoMuons > 140" + doTMcuts;
    up_bin_edge = 3;
    datanums = "12.50,1.48,1721.00,196.00,182.00,1042.00,7.00";
  }
  if(region == "R3"){
    cutsABCD = "invMassTwoMuons < 125 && deltaRTwoMuons > 3" + doTMcuts;
    cutsEFGH = "invMassTwoMuons > 125 && deltaRTwoMuons > 3" + doTMcuts;
    up_bin_edge = 1.3;
    datanums = "17.20,2.62,1045.00,104.00,109.00,1659.00,14.00";
  }

  std::vector<TH2D> signalNumsHist_mc16a = getNumEventsHists(fileName_mc16a,cutsABCD,cutsEFGH,up_bin_edge);
  std::vector<TH2D> signalNumsHist_mc16d = getNumEventsHists(fileName_mc16d,cutsABCD,cutsEFGH,up_bin_edge);
  std::vector<TH2D> signalNumsHist_mc16e = getNumEventsHists(fileName_mc16e,cutsABCD,cutsEFGH,up_bin_edge);

  TH2D histABCD_mc16a = signalNumsHist_mc16a[0];
  TH2D histEFGH_mc16a = signalNumsHist_mc16a[1];

  TH2D histABCD_mc16d = signalNumsHist_mc16d[0];
  TH2D histEFGH_mc16d = signalNumsHist_mc16d[1];
  
  TH2D histABCD_mc16e = signalNumsHist_mc16e[0];
  TH2D histEFGH_mc16e = signalNumsHist_mc16e[1];
  TH1::SetDefaultSumw2();

  TH2D * histABCD_ptr = &histABCD_mc16a;
  TH2D * histABCD = (TH2D*)histABCD_ptr->Clone("");
  histABCD->Add(&histABCD_mc16d);
  histABCD->Add(&histABCD_mc16e);

  TH2D * histEFGH_ptr = &histEFGH_mc16a;
  TH2D * histEFGH = (TH2D*)histEFGH_ptr->Clone("");
  histEFGH->Add(&histEFGH_mc16d);
  histEFGH->Add(&histEFGH_mc16e);

  TString A = to_string(histABCD->GetBinContent(1,1));
  TString B = to_string(histABCD->GetBinContent(1,3));
  TString C = to_string(histABCD->GetBinContent(3,1));
  TString E = to_string(histEFGH->GetBinContent(1,1));
  TString H = to_string(histEFGH->GetBinContent(3,3));
    
  TString Aerr = to_string(histABCD->GetBinError(1,1));
  TString Berr = to_string(histABCD->GetBinError(1,3));
  TString Cerr = to_string(histABCD->GetBinError(3,1));
  TString Eerr = to_string(histEFGH->GetBinError(1,1));
  TString Herr = to_string(histEFGH->GetBinError(3,3));

  float xSectionUncertUP, xSectionUncertDOWN;

  auto treeName = "trees_MEDIUMD0_";  
  ROOT::RDataFrame df(treeName, fileName_mc16a);
  auto df_range = df.Range(1);
  df_range.Foreach([&](float i, float j){ xSectionUncertUP = i; xSectionUncertDOWN=j;}, {"xSectionUncertUP","xSectionUncertDOWN"});
  
  TString output = datanums + "," + A + "," + Aerr + "," + B + "," + Berr + "," + C + "," + Cerr + "," + E + "," + Eerr +"," + H + "," +Herr + "," +  xSectionUncertUP + "," +  xSectionUncertDOWN;

  return output;

}

void scale_signal_MC(std::string file_mc16a, std::string file_mc16d, std::string file_mc16e, bool doLT, float oldLT, float newLT, bool doTM){
  fileName_mc16a = file_mc16a;
  fileName_mc16d = file_mc16d;
  fileName_mc16e = file_mc16e;
  doLTreweighting = doLT; 
  oldLifetime = oldLT;
  newLifetime = newLT;
  doTruthMatching = doTM;

  TString output_R1 = getoutputNums("R1");  
  TString output_R2 = getoutputNums("R2");
  TString output_R3 = getoutputNums("R3");    
  ofstream outtext("yields.txt", std::ofstream::out);

  if (outtext.is_open()){
    outtext << "number of estimated bkg events in H 0 | ";
    outtext << "stat uncert. for estimated bkg events in H 1 | "; 
    outtext << "number of real data events in A 2 | "; 
    outtext << "number of data events in B 3 | "; 
    outtext << "number of data events in C 4 | "; 
    outtext << "number of data events in E 5 | "; 
    outtext << "number of data events in H 6 | "; 
    outtext << "number of signal events in A 7 | "; 
    outtext << "stat uncert. for number of signal events in A 8 | "; 
    outtext << "number of signal events in B 9 | "; 
    outtext << "stat uncert. for number of signal events in B 10 | "; 
    outtext << "number of signal events in C 11 | "; 
    outtext << "stat uncert. for number of signal events in C 12 | "; 
    outtext << "number of signal events in E 13 | "; 
    outtext << "stat uncert. for number of signal events in E 14 | "; 
    outtext << "number of signal events in H 15 | "; 
    outtext << "stat uncert. for number of signal events in H 16 | ";
    outtext << "signal xsec uncert. up 17 | ";
    outtext << "signal xsec uncert. down 18 | \n";     
    outtext << output_R1 << "\n";
    outtext << output_R2 << "\n";
    outtext << output_R3 << "\n";
    outtext.close();
  }
  else cout << "Problem with opening file";
}
