#ifndef SCALE_SIGNAL_MC_H
#define SCALE_SIGNAL_MC_H

std::string fileName_mc16a = "";
std::string fileName_mc16d = "";
std::string fileName_mc16e = "";
bool doLTreweighting = 0;
bool doTruthMatching = 0;
float oldLifetime = 0;
float newLifetime = 0;
Double_t initalSumOfWeights =-9999;
#endif //SCALE_SIGNAL_MC_H
