#!/bin/bash

input_file_mc16a=${1}
input_file_mc16d=${2}
input_file_mc16e=${3}
doLifetimeReweighting=${4}
oldLifetime=${5}
newLifetime=${6}
doTruthMatchingMuonParent=${7}
root -b -l -q "source/scale_signal_MC.cxx(\"${input_file_mc16a}\", \"${input_file_mc16d}\", \"${input_file_mc16e}\", ${doLifetimeReweighting}, ${oldLifetime}, ${newLifetime}, ${doTruthMatchingMuonParent})";
