FROM rootproject/root:6.24.06-centos7
ADD . /get-yields
WORKDIR /get-yields

USER root

SHELL ["/bin/bash", "-c"]

#add nobody to the root group

RUN usermod -aG root nobody && \
    chsh -s /bin/bash nobody && \ 
    su nobody && \ 
    id -Gn

RUN chown -R nobody /get-yields