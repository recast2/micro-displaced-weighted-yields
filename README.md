The is the repo for the weighted events step. This is a root script that takes the signal ntuples and weights the signal sample to run 2. It spits out a txt file with the number of data events and number of signal events in regions A, B, C, E, and H for each Set of Regions: R1, R2, and R3.

Run with e.g.:
```
source runGetYields.sh mc16a.root mc16d.root mc16e.root 1 10 8 1
```

The first three inputs are the ntuples in each pileup campaign, then a bool to do lifetime reweighting, the old lifetime, the new lifetime to be reweighted  to, and a bool for truth matching the muon. The last option would need to be updated to if not running on the slepton signal samples of the micro displaced muons analysis.

If you don't have root installed you can run with a Docker image. first produce your ntuples with the micro-displaced-event-selection step. Then in the micro-displaced-weighted-yields directory:


```
docker pull rootproject/root:6.24.06-centos7
docker run --rm -it -v $PWD:/yields rootproject/root:6.24.06-centos7 bash
cd /yields
source runGetYields.sh mc16a.root mc16d.root mc16e.root 1 10 8 1
```

An example of not doing the lifetime re-weighting:
```
source runGetYields.sh mc16a.root mc16d.root mc16e.root 0 10 10 1
```